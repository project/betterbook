<?php
/**
 * @file
 * Administrative tasks
 */


/**
 * Redirect to add child of same node type page.
 */
function betterbook_add_child_page($node) {
  $type_url_str = str_replace('_', '-', $node->type);
  return drupal_goto('node/add/' . $type_url_str, array('query' => array('parent' => $node->nid)));
}

/**
 * Render the book outline.
 */
function betterbook_outline_page($node) {
  $instances = field_info_instances('node', $node->type);
  foreach ($instances as $name => $instance) {
    $field = field_info_field($name);
    if ($field['type'] == 'betterbook_book') {
      $items = field_get_items('node', $node, $field['field_name']);
      $item = $items[0];
      break;
    }
  }
  module_load_include('admin.inc', 'menu');
  $book = entity_load_single('betterbook', $item['bid']);
  $menu = menu_load($book->menu_name);
  return drupal_get_form('menu_overview_form', $menu);
}
