<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Child pages'),
  'description' => t('Get current nodes child pages.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'category' => t('Book'),
  'defaults' => array(
    'field' => '',
    'delta' => 0,
  ),
);

/**
 * Render the custom content type.
 */
function betterbook_childpages_content_type_render($subtype, &$conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return NULL;
  }
  $node = $context->data;
  // Get a shortcut to the book.
  $block = new stdClass();
  $block->delta = $node->nid;
  $block->content = array();
  $field = field_get_items('node', $node, $conf['field']);
  if ($field) {
    $item = $field[$conf['delta']];
    $book = entity_load_single('betterbook', $item['bid']);
    $mlid = $item['mlid'];
    if ($mlid == 0) {
      $block->content = menu_tree($book->menu_name);
    }
    else {
      $menu = menu_tree_all_data($book->menu_name);
      $child_menu = betterbook_childpages_menu($mlid, $menu);
      if ($child_menu !== FALSE) {
        $block->content = menu_tree_output($child_menu);
      }
    }
  }
  return $block;
}

function betterbook_childpages_menu($mlid, $menu) {
  foreach ($menu as $item) {
    if ($item['link']['mlid'] == $mlid && isset($item['below'])) {
      // Prune everything below this.
      $child_menu = $item['below'];
      foreach ($child_menu as &$item) {
        if (isset($item['below'])) {
          $item['below'] = FALSE;
        }
      }
      return $child_menu;
    }
    elseif(!empty($item['below'])) {
      $result = betterbook_childpages_menu($mlid, $item['below']);
      if ($result !== FALSE) {
        return $result;
      }
    }
  }
  return FALSE;
}


/**
 * Returns an edit form for custom type settings.
 */
function betterbook_childpages_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $fields = field_info_fields();
  $options = array();
  foreach ($fields as $name => $field) {
    if ($field['type'] == 'betterbook_book') {
      $options[$name] = $name;
    }
  }
  $form['field'] = array(
    '#type' => 'select',
    '#title' => t('Book field'),
    '#default_value' => $conf['field'],
    '#options' => $options,
  );
  $form['delta'] = array(
    '#type' => 'textfield',
    '#title' => t('Delta'),
    '#default_value' => $conf['delta'],
  );
  return $form;
}

/**
 * Submit handler for the custom type settings form.
 */
function betterbook_childpages_content_type_edit_form_submit(&$form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Returns the administrative title for a type.
 */
function betterbook_childpages_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" book child pages', array('@s' => $context->identifier));
}
