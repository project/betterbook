<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Book navigation'),
  'description' => t('Basis for the book navigation.'),
  'required context' => new ctools_context_required(t('Book'), 'entity:betterbook'),
  'category' => t('Book'),
  'defaults' => array(),
);

/**
 * Render the custom content type.
 */
function betterbook_navigation_content_type_render($subtype, &$conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return NULL;
  }
  $block = new stdClass();
  // Get a shortcut to the book.
  $book = $context->data;
  $block->delta = $book->bid;
  $block->content = menu_tree($book->menu_name);
  $uri = entity_uri('betterbook', $book);
  $block->title = l(entity_label('betterbook', $book), $uri['path']);
  $block->content = empty($block->content) ? ' ' : $block->content;
  return $block;
}

/**
 * Returns an edit form for custom type settings.
 */
function betterbook_navigation_content_type_edit_form($form, &$form_state) {
  // $conf = $form_state['conf'];
  return $form;
}

/**
 * Submit handler for the custom type settings form.
 */
function betterbook_navigation_content_type_edit_form_submit(&$form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Returns the administrative title for a type.
 */
function betterbook_navigation_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" book navigation', array('@s' => $context->identifier));
}
