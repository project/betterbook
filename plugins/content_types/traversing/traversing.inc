<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Book traversing links'),
  'description' => t('Links to traverse the tree.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'category' => t('Book'),
  'defaults' => array(
    'field' => '',
    'delta' => 0,
  ),
);

/**
 * Render the custom content type.
 */
function betterbook_traversing_content_type_render($subtype, &$conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return;
  }
  $node = $context->data;
  // Get a shortcut to the book.
  $block = new stdClass();
  $block->delta = $node->nid;
  $block->content = array();
  $field = field_get_items('node', $node, $conf['field']);
  $links = array();
  if ($field) {
    $item = $field[$conf['delta']];
    $book = entity_load_single('betterbook', $item['bid']);
    $menu = menu_tree_all_data($book->menu_name);
    $flat = array();
    _betterbook_flatten_menu($menu, $flat);
    if (isset($flat[$item['mlid']]['prev'])) {
      $links[] = array('href' => $flat[$item['mlid']]['prev']['link_path'], 'title' => $flat[$item['mlid']]['prev']['link_title'], 'attributes' => array('class' => array('prev')));
    }
    elseif($item['mlid'] != 0) {
      $uri = entity_uri('betterbook', $book);
      $links[] = array('href' => $uri['path'], 'title' => entity_label('betterbook', $book), 'attributes' => array('class' => array('prev')));
    }
    if (!empty($flat[$item['mlid']]['plid'])) {
      $links[] = array('href' => $flat[$flat[$item['mlid']]['plid']]['link_path'], 'title' => t('Up'), 'attributes' => array('class' => array('up')));
    }
    if (isset($flat[$item['mlid']]['next'])) {
      $links[] = array('href' => $flat[$item['mlid']]['next']['link_path'], 'title' => $flat[$item['mlid']]['next']['link_title'], 'attributes' => array('class' => array('next')));
    }
    elseif($item['mlid'] == 0) {
      $first = current($flat);
      $links[] = array('href' => $first['link_path'], 'title' => $first['link_title'], 'attributes' => array('class' => array('next')));
    }
  }
  if (count($links)) {
    $block->content['links'] = array(
      '#markup' => theme('links', array('links' => $links)),
    );
  }
  return $block;
}

/**
 * Returns an edit form for custom type settings.
 */
function betterbook_traversing_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $fields = field_info_fields();
  $options = array();
  foreach ($fields as $name => $field) {
    if ($field['type'] == 'betterbook_book') {
      $options[$name] = $name;
    }
  }
  $form['field'] = array(
    '#type' => 'select',
    '#title' => t('Book field'),
    '#default_value' => $conf['field'],
    '#options' => $options,
  );
  $form['delta'] = array(
    '#type' => 'textfield',
    '#title' => t('Delta'),
    '#default_value' => $conf['delta'],
  );
  return $form;
}

/**
 * Submit handler for the custom type settings form.
 */
function betterbook_traversing_content_type_edit_form_submit(&$form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Returns the administrative title for a type.
 */
function betterbook_traversing_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" book traversing links', array('@s' => $context->identifier));
}
