<?php

/**
 * @file
 * Plugin to provide an relationship handler for betterbook parent.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Betterbook parent'),
  'keyword' => 'betterbook_parent',
  'description' => t('Adds a betterbook parent from a node context.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'context' => 'betterbook_parent_context',
  'edit form' => 'betterbook_parent_settings_form',
  'defaults' => array('type' => 'top', 'field' => NULL, 'delta' => 0),
);

/**
 * Return a new context based on an existing context.
 */
function betterbook_parent_context($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('node');
  }
  $node = $context->data;

  $field = field_get_items('node', $node, $conf['field']);

  if ($field) {
    $item = $field[$conf['delta']];
    $book = entity_load_single('betterbook', $item['bid']);
    $menu_link = menu_link_load($item['mlid']);

    if ($conf['type'] == 'top') {
      $nid = $book['entity_id'];
    }
    elseif ($conf['type'] == 'parent') {
      // Just load the parent book.
      $item = menu_link_load($menu_link['plid']);
      $nid = str_replace('node/', '', $item['link_path']);
    }
    else {
      $item = menu_link_load($menu_link[$conf['type']]);
      $nid = str_replace('node/', '', $item['link_path']);
    }

    if (!empty($nid)) {
      // Load the node.
      $node = node_load($nid);
      // Generate the context.
      return ctools_context_create('node', $node);
    }
  }
  return ctools_context_create_empty('node');
}

/**
 * Settings form for the relationship.
 */
function betterbook_parent_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $fields = field_info_fields();
  $options = array();
  foreach ($fields as $name => $field) {
    if ($field['type'] == 'betterbook_book') {
      $options[$name] = $name;
    }
  }
  $form['field'] = array(
    '#type' => 'select',
    '#title' => t('Book field'),
    '#default_value' => $conf['field'],
    '#options' => $options,
  );
  $form['delta'] = array(
    '#type' => 'textfield',
    '#title' => t('Delta'),
    '#default_value' => $conf['delta'],
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Relationship type'),
    '#options' => array(
      'top' => t('Top level book'),
      'parent' => t('Immediate parent'),
      'p2' => t('Parent 2'),
      'p3' => t('Parent 3'),
      'p4' => t('Parent 4'),
      'p5' => t('Parent 5'),
      'p6' => t('Parent 6'),
      'p7' => t('Parent 7'),
      'p8' => t('Parent 8'),
      'p9' => t('Parent 9'),
      ),
    '#default_value' => $conf['type'],
  );

  return $form;
}
