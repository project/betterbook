(function ($) {
  Drupal.behaviors.betterbookChangeItems = {
    attach: function (context, settings) {
      $('.book-select').change(updateBookMenu);
      $('.book-select').each(updateBookMenu);
      function updateBookMenu() {
        // Update list of available parent menu items.
        var field_context = $(this).parent().parent();
        var val = $(this).val();
        if (val == '_new') {
          $('.menu-parent-select').parent().hide();
        }
        else {
          $('.menu-parent-select').parent().show();
          var menu = settings.betterbook.book_menus[val];
          Drupal.betterbook.update_parent_list(field_context, menu);
        }
      }
    }
  };

  Drupal.betterbook = {};

  /**
   * Function to set the options of the menu parent item dropdown.
   */
  Drupal.betterbook.update_parent_list = function (context, menu) {
    var values = [menu];
    var url = Drupal.settings.basePath + 'betterbook/menu/parents';
    $.ajax({
      url: location.protocol + '//' + location.host + url,
      type: 'POST',
      data: {'menus[]' : values},
      dataType: 'json',
      success: function (options) {
        // Save key of last selected element.
        var selected = $('.menu-parent-select :selected').val();
        // Remove all exisiting options from dropdown.
        $('.menu-parent-select').children().remove();
        // Add new options to dropdown.
        jQuery.each(options, function(index, value) {
          $('.menu-parent-select').append(
            $('<option ' + (index == selected ? ' selected="selected"' : '') + '></option>').val(index).text(value)
          );
        });
      }
    });
  };
})(jQuery);
